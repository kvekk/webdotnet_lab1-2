﻿using LinkedList;

string dividingLine = new string('-', 50);
MyLinkedList<int> list = new(Enumerable.Repeat(0, 5).Select(x => new Random().Next(0, 100)));
Console.WriteLine("MyLinkedList was instantiated. It copied IEnumerable's elements into itself.");
PrintList(list);
Console.WriteLine("We can also create an empty list.");
MyLinkedList<int> emptyList = new();
PrintList(emptyList);
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's test AddFirst(T), AddLast(T), " +
    "AddFirst(MyLinkedListNode<T>, AddLast(MyLinkedListNode<T>).");
list.AddFirst(101);
var unusedNode = list.AddLast(909);
MyLinkedListNode<int> node1 = new(99);
MyLinkedListNode<int> node2 = new(44);
list.AddFirst(node1);
list.AddLast(node2);
PrintList(list);
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's try to add a node, which is already attached to the list.");
try
{
    list.AddLast(node2); // doesn't matter if it's AddLast, AddFirst, AddBefore or AddAfter
}
catch
{
    Console.WriteLine("Handling exception. The node must not be attached to any list to proceed with this operation.");
}
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's try to add a node, which is not attached to any list.");
MyLinkedListNode<int> node3 = new(10560);
list.AddLast(node3);
PrintList(list);
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's check LinkedList's and LinkedListNode's properties.");
Console.WriteLine($"  > node2. Value: {node2.Value}, Next: {node2.Next}, Previous: {node2.Previous}");
Console.WriteLine($"  > list. Count: {list.Count}, First: {list.First}, Last: {list.Last}");
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's clear the list.");
list.Clear();
PrintList(list);
Console.WriteLine("Now, let's add node1, node2, node3.");
list.AddLast(node1);
list.AddLast(node2);
list.AddLast(node3);
PrintList(list);
Console.WriteLine("Now, let's try AddBefore, AddAfter.");
list.AddBefore(node1, 100);
list.AddAfter(node3, 555);
list.AddBefore(node2, 1);
list.AddAfter(node2, 2);
PrintList(list);
Console.WriteLine("Now, let's try to add an item after a node, which is not attached to our list.");
try
{
    list.AddAfter(unusedNode, 88); // it could've also been AddBefore
}
catch
{
    Console.WriteLine("Handling exception. The node is either unattached or attached to another list.");
}
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's try to RemoveFirst and RemoveLast.");
list.RemoveFirst();
list.RemoveLast();
PrintList(list);
Console.WriteLine("Now, let's try to RemoveFirst and RemoveLast on an empty list.");
try
{
    emptyList.RemoveFirst();
}
catch
{
    Console.WriteLine("Handling exception. This list is empty, so RemoveFirst is a no-no.");
}
try
{
    emptyList.RemoveLast();
}
catch
{
    Console.WriteLine("Handling exception. This list is empty, so RemoveLast is a no-no, too.");
}

Console.WriteLine("Now, let's try to remove a node with value '1'");
if (list.Remove(1))
{
    Console.WriteLine("  > SUCCESS");
    PrintList(list);
}

Console.WriteLine("Now, let's try to remove a node with value '1811'");
if (!list.Remove(1811))
{
    Console.WriteLine("There's no node with such a value.");
}
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's remove a particular node (node2).");
list.Remove(node2);
PrintList(list);
Console.WriteLine("Now, let's try to remove a node, which doesn't belong to the list.");
try
{
    list.Remove(unusedNode);
}
catch
{
    Console.WriteLine("Handling exception. The node is either unattached or attached to another list.");
}
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's add some values and try Find and FindLast.");
list.AddLast(10);
list.AddLast(10);
list.AddLast(10);
list.AddLast(10);
PrintList(list);
MyLinkedListNode<int>? foundNode = list.Find(10);
MyLinkedListNode<int>? foundLastNode = list.FindLast(10);
Console.WriteLine($"list.Find(10) -> Value: {foundNode}, Previous: {foundNode!.Previous}, " +
    $"Next: {foundNode!.Next}");
Console.WriteLine($"list.FindLast(10) -> Value: {foundLastNode}, Previous: {foundLastNode!.Previous}, " +
    $"Next: {foundLastNode!.Next}");
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's check Contains.");
Console.WriteLine($"Our list contains 1811: {list.Contains(1811)}");
Console.WriteLine($"Our list contains 2: {list.Contains(2)}");
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's check CopyTo.");
int[] arr = new int[10];
try
{
    list.CopyTo(arr, 11);
}
catch
{
    Console.WriteLine("Handling exception. Such an index does not exist.");
}
try
{
    list.CopyTo(arr, -5);
}
catch
{
    Console.WriteLine("Handling exception. Such an index does not exist.");
}
try
{
    list.CopyTo(arr, 5);
}
catch
{
    Console.WriteLine("Handling exception. The data cannot fit into the array.");
}
list.CopyTo(arr, 2);
Console.Write("Array values: ");
foreach (var x in arr)
{
    Console.Write(x + " ");
}
Console.WriteLine();
Console.WriteLine(dividingLine);

Console.WriteLine("Now, let's check modifying our list in the 'foreach loop'");
try
{
    foreach (var x in list)
    {
        list.AddLast(x);
    }
}
catch
{
    Console.WriteLine("Handling exception. Don't mutate the collection in a 'foreach loop'!");
}

Console.WriteLine();
Console.WriteLine(dividingLine);
Console.WriteLine(dividingLine);
Console.WriteLine();

Console.WriteLine("NOW, LET'S CHECK EVENTS.");
list.Notify += DisplayMessage;
//list.Notify += Display2nd;
list.Clear();
PrintList(list);
list.AddFirst(1);
var node = list.AddLast(4);
list.AddFirst(9);
PrintList(list);
list.Remove(node);
PrintList(list);
list.RemoveFirst();
PrintList(list);

static void PrintList<T>(MyLinkedList<T> list)
{
    Console.Write("  > List values: ");
    foreach (T item in list)
    {
        Console.Write(item + " ");
    }
    Console.WriteLine();
}

void DisplayMessage(string message)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine(message);
    Console.ResetColor();
}

void Display2nd(string message)
{
    Console.WriteLine("2nd");
}